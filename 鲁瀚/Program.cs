﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 面向对象练习
{
    class Program
    {
        static void Main(string[] args)
        {
            Student zs = new Student(19, "张三", 60, 69, 88);
            zs.Hello();
            zs.Average();

            Student xl = new Student(18,"小林",44,84,99);
            xl.Hello();
            xl.Average();

            Student lh = new Student(19, "鲁瀚");
            lh.Hello();
            
            Console.ReadKey();
        }
    }
}
