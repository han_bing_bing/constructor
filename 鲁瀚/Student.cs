﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 面向对象练习
{
    public class Student
    {
        public Student ()
        {

        }

        //构造函数重载
        public Student(int age ,string name ) : this(age, name, 0, 0, 0) 
        {
            //this.Age = age;
            //this.Name = name;
        }
        public Student (string name ,int age)
        {

        }

        public Student(int age, string name, float cs, float es, float ms)
        {
            this.Age = age;
            this.Name = name;
            this.Cs = cs;
            this.Es = es;
            this.Ms = ms;
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private char gender;

        public char Gender
        {
            get
            {
                if (gender != '男' && gender != '女')
                {
                    gender = '男';

                }
                return gender;
            }
            set { gender = value; }
        }

        private int age;

        public int Age
        {
            get { return age; }
            set
            {
                if (value < 0 || value > 120)
                {
                    value = 0;
                }
                age = value;
            }
        }

        private float cs;

        public float Cs
        {
            get { return cs; }
            set { cs = value; }

        }

        private float ms;

        public float Ms
        {
            get { return ms; }
            set { ms = value; }
        }

        private float es;

        public float Es
        {
            get { return es; }
            set { es = value; }
        }

        public void Hello()
        {
            Console.WriteLine("我的名字叫{0},我今年{1}，我是{2}同学 ", this.Name, this.Age, this.Gender);
        }

        public void Average()
        {
            Console.WriteLine("我的名字叫{0},我的总分数是{1},我的平均分是{2}", this.Name, this.Cs + this.Es + this.Ms, (this.Cs + this.Es + this.Ms) / 3);
        }


    }
}
