﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class Student
    {
        public Student()
        {
            Console.WriteLine("这是一个构造函数");

        }
        
        public int Age { get; set; }
        public string Name { get; set; }
        public double Point { get; set; }
        public  Student(int age,string name,double point)
        {
            Age = age;
            Name = name;
            Point = point;
            Console.WriteLine("三个参数构造方法");
        }
        public void StudnetInfo()
        {
            Console.WriteLine("年龄{0} 名字{1} 分数{2}", Age,Name,Point);
            Console.WriteLine();
        }
        ~Student()
        {
            Console.WriteLine("这是一个析构函数,随便打点啥");
        }


        public Student(int age,string name) : this(age,name, 0)
        {
        }




        
    }
}
