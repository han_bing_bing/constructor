﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Student
    {
        private int _id;
        public int Id {
            get { return _id; }
            set { _id = value; }
         }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private char _sex;
        public char Sex
        {
            get { return _sex; }
            set {
                if (value!='男'||value!='女')
                {
                    value = '男';
                }
                _sex = value; }
        }
       public   Student(int id,string name,char sex)
        {
            this.Id = id;
            this._name = name;
            this.Sex = sex;
            
        }


        public Student(int id) : this(0,"", '女')
        {
            this.Id = id;
        }
    public static void Speak(int num1,int num2)
        {
            Console.WriteLine("方法1,两数之和为{0}",num1+num2);
        }
        public static void Speak(int num1,int num2,int num3)
        {
            Console.WriteLine("方法2,三数之和为{0}", num1 + num2 + num3);
        }
        public void Text()
        {
    
            Console.WriteLine("我叫{0}，我名为是{1}.我是{2}生", this.Name,this.Id,this.Sex);
        }

    }
}
