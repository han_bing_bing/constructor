﻿using System;

namespace 构造方法
{
    class Structure
    {
        public static void Main(string[] args)
        {
            Person person = new Person("Joker",18);
            Student student = new Student("Fox",18, "XJ");
            Graduate graduate = new Graduate(20,"Queen","XJ",20);
        }
    }
    class Person
    {


        string Name;
        int Age;
        public Person() { }
        public Person(string Name, int Age)
        {
            this.Age = Age;
            this.Name = Name;
            Console.WriteLine("这个人名称："+Name);
            Console.WriteLine("这个人年龄："+Age);
        }
    }
    class Student : Person
    {
        string School;

        public Student() : this(null, 0, null)
        {

        }
        public Student(string Name,int Age,string School):base(Name,Age)
        {
            this.School = School;
            //Console.WriteLine("这个学生名字:" + Name);
            //Console.WriteLine("这个学生年龄:"+Age);
            Console.WriteLine("这个学生学校:"+School);
        }
    }
    class Graduate : Student
    {
        int GraduateAge;
        public Graduate():this(0,null,null,0)
        {

        }
        public Graduate(int Age,string Name,string School,int GraduateAge):base(Name,Age,School)
        {
            this.GraduateAge = GraduateAge;
            Console.WriteLine("这个毕业生年龄为:"+GraduateAge);
        }
    }

}
