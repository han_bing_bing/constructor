﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Student
    {
        public Student (string name,string sex,string tel)
        {
            this.Name = name;
            this.Sex = sex;
            this.Tel = tel;
        }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string Tel { get; set; }
        public void PrintWkl()
        {
            Console.WriteLine("姓名:"+this.Name);
            Console.WriteLine("性别:"+this.Sex);
            Console.WriteLine("电话:"+this.Tel);
        }
    }
}
