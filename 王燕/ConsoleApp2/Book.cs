﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace ConsoleApp2
{
    class Book
    {
        public Book(int x)
        {
            if (x >= 90)
            {
                Console.WriteLine("等级：优");
            }
            else if (x < 90 && x >= 60)
            {
                Console.WriteLine("等级：良");
            }
            else
            {
                Console.WriteLine("等级：差");
            }
        }
        public Book(int x, int y)
        {
            if (x + y >= 180)
            {
                Console.WriteLine("等级：优");
            }
            else if (x + y < 180 && x + y >= 120)
            {
                Console.WriteLine("等级：良");
            }
            else
            {
                Console.WriteLine("等级：差");
            }
        }

        public Book(int x, int y, int z)
        {
            if (x + y + z >= 270)
            {
                Console.WriteLine("等级：优");
            }
            else if (x + y + z < 270 && x + y + z >= 180)
            {
                Console.WriteLine("等级：良");
            }
            else
            {
                Console.WriteLine("等级：差");
            }
        }

        public char Sex { get; set; }
        public int Id { get; set; }
        public double Score { get; set; }
        public int ClassId { get; set; }
        public string Name { get; set; }


        public Book(char sex, string name, int id, int classid, double score)
        {
            Id = id;
            Name = name;
            Sex = sex;
            ClassId = classid;
            Score = score;
        }
        public void Print()
        {
            Console.WriteLine(" 序号：{0}  用户名：{1}   性别：{2}   班级：{3}班  分数：{4}", Id,Name,Sex,ClassId,Score);
        }

        public Book(char sex,string name):this(sex,name,0,0,0)
        {

        }
        public Book(char sex) : this(sex,"无名", 0, 0, 0)
        {

        }

        ~Book()
        {
            Console.WriteLine("我会帮你清理垃圾的");
        }
    }
}
