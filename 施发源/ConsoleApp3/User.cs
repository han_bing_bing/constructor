﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    public class User
    {
        public User(string name,string password,string tel)//构造方法
        {
            this.Name = name;
            this.Password = password;
            this.Tel = tel;
        }
        ~User()//析构方法
        {
            Console.WriteLine("调用成功！");
        }
        public string Name { get; set; }//属性
        public string Password { get; set; }//属性
        public string Tel { get; set; }//属性

        public void printus()//输出
        {
            Console.WriteLine($"用户名：{this.Name}    密码：{this.Password}    电话：{this.Tel}");
        }

    }
}
