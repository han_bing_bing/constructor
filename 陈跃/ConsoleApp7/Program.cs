﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student(19, "陈跃", 100);
            student.PrintStudentInfo();

            Student student1 = new Student(19, "郑凯", 80);
            student1.PrintStudentInfo();
        }
    }
}
