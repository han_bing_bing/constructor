﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    public class Student
    {
        public int Age { get; set; }

        public string Name { get; set; }

        public int Score { get; set; }

        public Student(int age,string name,int score)
        {
            Age = age;
            Name = name;
            Score = score;
            Console.WriteLine("远赴人间惊鸿宴，一睹人间盛世颜，欢迎来到闽大");
           
        }

        public Student(int age, string name) : this(age, name, 80)
        {

        }

        public void PrintStudentInfo()
        {
            Console.WriteLine("姓名：{0}，年龄：{1}，成绩：{2}",Name,Age,Score);
            Console.WriteLine();
        }

    }
}
