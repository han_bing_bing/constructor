﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
     public class Student
    {
        //构造函数
        public string name;
        public int studentCode;
        public int className;

        public  Student(string name,int studentCode,int className)
        {
            this.name = name;
            this.studentCode = studentCode;
            this.className = className;
        }


        //析构函数
        ~Student()
        {
            Console.WriteLine("能用吗");
        }


        //构造函数重载
        public Student (string name ,int studentCode):this(name,studentCode, 2)
        {

        }

        public Student (string name):this (name, 112, 1)
        {

        }

        public void StudentInfo()
        {
            Console.WriteLine("学生姓名：{0},学号:{1}，班级：{2}",name,studentCode,className );
            
        }

    }
}
