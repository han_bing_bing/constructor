﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 练习_构造函数
{
    class Student
    {
        private int _id;

        private string _name;

        private int _age;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;

            }
            set
            {
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        public Student(int Id,string Name,int Age)
        {
            _id = Id;
            _name = Name;
            _age = Age;


        }
        public void PrintStudentInfo()
        {
            Console.WriteLine("学号:{0}，姓名:{1}，年龄:{2}",Id,Name,Age);
        }


    }
}
