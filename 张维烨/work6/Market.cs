﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work6
{
    class Market
    {
        private string name;
        private int price;

        public Market(string name, int price)
        {
            this.Name = name;
            this.Price = price;
        }

        public string Name { get { return name; } set { name = value; } }
        public int Price { get { return price; } set { price = value; } }

        public void PrintMesg()
        {
            Console.WriteLine("{0},{1}元/斤\n", this.Name, this.Price);
        }
        public void Cabbage(string name)
        {
            Console.WriteLine("我是：{0}", name);
        }
        public void Cabbage(string v, short Content)
        {
            Console.WriteLine("我是大白菜：100克含量里有{0} {1}毫克", v, Content);
        }
        public Market(int id) : this("卷心菜", 1)
        {
            Console.WriteLine("实际上我是{0}，价格{1}元，我记得我是{2}号商品\n", name, price, id);
        }
        ~Market()
        {
            Console.WriteLine("\n垃圾回收车来了~\n");
        }
    }

}
