﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Student
    {

        public string Name { set; get; }
        public int Age { set; get; }
        public int Score { set; get; }


        public Student(string name , int age , int score)
        {
            Name = name;
            Age = age;
            Score = score;
            Console.WriteLine("三个参数");
        }

        public Student (string name , int age ) : this(name , age , 0)
        {

        }

        public Student(string name) : this(name, 0, 0)
        {

        }
        public void PrintStudenInfo()
        {
            Console.WriteLine("姓名: {0} , 年龄: {1} , 成绩: {2}", Name, Age, Score);
        }

        ~Student()
        {
            Console.WriteLine("这是个析构函数");
        }
    }
}
