﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class SumUtils
    {
        public int Sum(int a , int b)
        {
            Console.WriteLine("你调用的是整形加法函数");
            return a + b;
        }

        public double Sum(double a, double b)
        {
            Console.WriteLine("你调用的是双精度浮点型加法函数"); 
            return a + b;
        }

        public string Sum(string a , string b)
        {
            Console.WriteLine("你调用的是字符串加法 | 连接函数");
            return a + b;
        }
    }
}
