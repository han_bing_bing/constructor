﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student("阿右",18,90);
            student.PrintStudenInfo();

            Student student1 = new Student("阿左", 19);
            student1.PrintStudenInfo();

            Student student2 = new Student("阿中");
            student2.PrintStudenInfo();

            SumUtils sumUtils = new SumUtils();

            
            Console.WriteLine(sumUtils.Sum(500, 20));
            Console.WriteLine(sumUtils.Sum(50, 2.1));
            Console.WriteLine(sumUtils.Sum("“秋千水，竹马道”", "“一眼见你万物不及”") );

           

            
        }
    }
}
