﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Student
    {
        public Student(int id,string name,int age)
        {
            _id = id;
            _name = name;
            _age = age;
        }
        public int _id { get; set; }
        public string _name { get; set; }
        public int _age { get; set; }

        public void PrintStudent()
        {
            Console.WriteLine("我的名字是"+this._name);
            Console.WriteLine("我的学号是"+this._id);
            Console.WriteLine("我的年龄是"+this._age);
        }
        ~Student()
        {
            Console.WriteLine("调用了析构方法");
        }
    }
}
