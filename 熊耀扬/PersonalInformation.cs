﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ConsoleApp4
{
    class PersonalInformation
    {
        public PersonalInformation(string name, int height, int weight)
        {
            Name = name;
            Height = height;
            Weight = weight;
        }
        public PersonalInformation(string name, int height) : this("李四", 181, 0)
        {
            
        }
        public PersonalInformation() : this("查无此人", 0, 0)
        {
            
        }
        public string Name { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public void Personal()
        {
            Console.WriteLine("姓名: {0} 身高: {1}cm 体重: {2}kg",this.Name,this.Height,this.Weight);
        }
    }
}
