﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    class Addition
    {
        public int Add(int a,int b)
        {
            return a + b;
        }
        public double Add(double a, double b)
        {
            return a + b;
        }
        public string Add(string a, string b)
        {
            return a + b;
        }
    }
}
