﻿using System;
using System.IO;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonalInformation personalInformation = new PersonalInformation("张三",178,61);
            personalInformation.Personal();
            PersonalInformation personalInformation1 = new PersonalInformation("李四",181);
            personalInformation1.Personal();
            PersonalInformation personalInformation2 = new PersonalInformation();
            personalInformation2.Personal();


            Addition addition = new Addition();
            Console.WriteLine(addition.Add(6 , 8));
            Console.WriteLine(addition.Add(1.3 , 5.4));
            Console.WriteLine(addition.Add("鲨" , "鱼"));
        }
    }
}
