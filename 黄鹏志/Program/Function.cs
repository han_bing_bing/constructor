﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Function
    {
        public string name;
        int age;
        protected string sex;

        ~Function()
        {
            Console.WriteLine("调用了析构函数,这个没有参数我跟你讲");
        }
        public Function(string xm, int nl,string se)
        {
            name = xm;
            age = nl;
            sex = se;
        }

        public void ShowPersonMsg()
        {
            Console.WriteLine("姓名:{0}，年龄:{1}岁，性别{2}", name, age,sex );
        }
    }
}
