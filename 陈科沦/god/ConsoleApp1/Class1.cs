﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
   public class Student
    {
      public string Name { get; set; }
      public int Age { get; set; } 
      public int Height { get; set; }

        public Student(string name,int age, int height)
        {
            Name = name;
            Age = age;
            Height = height;
        }

        public void PrintStudentInfo()
        {
            Console.WriteLine("姓名：{0}，年龄：{1}，身高：{2}", Name, Age, Height);
            Console.WriteLine();
        }
    }
}
