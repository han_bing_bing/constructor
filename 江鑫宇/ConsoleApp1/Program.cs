﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            Console.WriteLine("请输入你的年龄：");
            student.age = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入你的姓名：");
            student.name = Convert.ToString(Console.ReadLine());
            Console.WriteLine("请输入你的爱好：");
            student.hobby = Convert.ToString(Console.ReadLine());
            student.personal();
        }
    }
}
