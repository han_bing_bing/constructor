﻿using System;

namespace 构造函数
{
    class Program
    {
        static void Main(string[] args)
        {
            Person Li = new Person();
            Console.WriteLine("个人信息表");
            Li.name = "Bruse Li";
            Li.age = 19;
            Li.weight = 110;
            Li.height = 165;

            Li.information();

            Person Huang = new Person();
            Huang.message("Huang", 19, 170.9, 120.5);
            Console.ReadKey();
        }

        ~Program()

        {

            //在析构函数中，可以执行释放其他托管资源处理

            Console.WriteLine("当前Car对象被释放,CarID={CarID}");




        }
    }
}
