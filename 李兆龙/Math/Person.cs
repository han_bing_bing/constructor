using System;
using System.Collections.Generic;
using System.Text;

namespace 构造函数
{
    class Person
    {
        public string name;
        public int age;
        public double height;
        public double weight;

        public string Name { get; set; }
        public int Age { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
                  
        public void GetName()
        {
            Console.WriteLine("姓名：{0} 年龄：{1} 身高：{2}  体重：{3}",name,age,height,weight);
        }

        public void GetMsg(string name,int age,double height,double weight)
        {
            this.Name = name;
            this.Age = age;
            this.Height = height;
            this.Weight = weight;
            Console.WriteLine("姓名：{0} 年龄:{1} 身高：{2} 体重：{3}",this.Name,this.Age,this.Height,this.Weight);
        }

        ~Person()
        {
            Console.WriteLine("我被调用了");
        }

    }
}
