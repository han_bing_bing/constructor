﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            //调用对象的构造函数进行初始化对象
            Student mike = new Student("迈克",7,'男',98,97);      
            mike.Describe();
            mike.Score();
            Console.WriteLine();

            Student jane = new Student("简", 18, '女', 98, 99);
            jane.Describe();
            jane.Score();
            
        }
    }
}
