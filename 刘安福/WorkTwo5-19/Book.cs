﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTwo5_19
{
    public class Book
    {

        public Book(int id,string title ,int price)
        {
            Console.WriteLine("调用钢铁侠");

            Id = id;
            Title = title;
            Price = price;
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public int Price { get; set; }

        public void PrintBook()
        {
            Console.WriteLine("书的Id：{0} ， 书名《{1}》 ，书价：{2} ",this.Id,this.Title,this.Price);
        }

        //public void Setup(int id,string title,int price)
        //{
        //    this.Id = id;
        //    this.Title = title;
        //    this.Price = price;
        //}
        

       
        ~Book()
        {
            Console.WriteLine("我买的书：《 {0} 》，被卖了，两毛一斤的那种！！",Title);
            Console.WriteLine();
        }
        
    }
}
