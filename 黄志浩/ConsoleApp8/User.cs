﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class User
    {
        public User(string name, string password, string phone)
        {
            this.Name = name;
            this.Password = password;
            this.Phone = phone;
        }

        public string Name { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }

        public void PrintMsg()
        {
            Console.WriteLine("用户名："+Name);
            Console.WriteLine("密码："+Password);
            Console.WriteLine("联系方式："+Phone);
        }

        ~User()
        {
            Console.WriteLine("看看有没有显示，这是一个析构方法");
        }

        public int Sum(int a, int b)
        {
            Console.WriteLine("所以说8+4另一边看不到咯？");
            return a + b;
           
        }
        public string Sum(string a, string b)
        {
            Console.WriteLine("今天来点小鸟伏特加来这么高");
            return a + b;
          
        }
    }
}
